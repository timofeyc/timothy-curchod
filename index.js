// Dependencies, express, js & ejs
var express = require('express');
var app = express();
var fs = require('fs');
var data = require('./data.js');
// Setup
app.set('port', (process.env.PORT || 5000));
app.use(express.static(__dirname + '/'));
app.set('views', __dirname + '/');
app.engine('html', require('ejs').renderFile);
app.listen(app.get('port'), function() {
  console.log('Node app is running on port', app.get('port'));
});
// Routes
app.get('/', function(request, response) {
  response.render('./index.html');
});

app.get('/data', function (req, res) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  res.setHeader('Content-Type', 'application/json');
  
  console.log('len',data.data);
  res.send(data.data);
});