/// <reference path="../typings/angularjs/angular.d.ts" />
var ampApp;
(function (ampApp) {
    'use strict';
    angular.module('ampApp', [])
        .controller('MainCtrl', ampApp.MainCtrl);
    angular.module('ampApp')
        .service('Service', ampApp.Service);
})(ampApp || (ampApp = {}));
