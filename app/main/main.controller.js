/// <reference path="../../typings/angularjs/angular.d.ts" />
var ampApp;
(function (ampApp) {
    'use strict';
    var Person = (function () {
        function Person(id, firstName, lastName, picture, Title) {
            this.id = id;
            this.firstName = firstName;
            this.lastName = lastName;
            this.picture = picture;
            this.Title = Title;
        }
        return Person;
    })();
    var MainCtrl = (function () {
        function MainCtrl($scope, Service) {
            var _this = this;
            this.Service = Service;
            $scope.vm = this;
            $scope.vm.people = new Array();
            this.isLoading = true;
            $scope.vm.loadingText = 'Loading';
            $scope.vm.searchResults;
            this.Service.getPeople().then(function (data) {
                data.forEach(function (eachPerson) {
                    var newImage = eachPerson.picture.replace('.jpg', '.png');
                    eachPerson.picture = newImage;
                    $scope.vm.people.push(eachPerson);
                });
                _this.isLoading = false;
                _this.loadingText = 'Done';
            }, function (reason) {
                _this.loadingText = 'Loading data failed: reason - ' + reason;
                console.log(_this.loadingText);
            }, function (update) {
                _this.loadingText = 'Got notification: update - ' + update;
                console.log(_this.loadingText);
            });
        }
        return MainCtrl;
    })();
    ampApp.MainCtrl = MainCtrl;
})(ampApp || (ampApp = {}));
