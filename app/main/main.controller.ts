/// <reference path="../../typings/angularjs/angular.d.ts" />
module ampApp {
  'use strict';
  
  class Person {
    public id: number;
    public firstName: string;
    public lastName: string;
    public picture: string;
    public Title: string;
    constructor(id: number, firstName: string, lastName: string, picture: string, Title: string) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.picture = picture;
        this.Title = Title;
    }
  }
  
  export interface IMainScope extends ng.IScope {
      vm: ampApp.MainCtrl;
      people: Person[];
  }
  export class MainCtrl {
      public test;
      public people;
      public searchText;
      public loadingText;
      public isLoading;
      public searchResults;
    constructor (
        $scope: IMainScope, 
        private Service: ampApp.Service) {
            $scope.vm = this;
           $scope.vm.people = new Array<Person>();
            this.isLoading = true;
            $scope.vm.loadingText = 'Loading';
            $scope.vm.searchResults;
            this.Service.getPeople().then((data) => {
                data.forEach(function(eachPerson: Person) {
                    var newImage = eachPerson.picture.replace('.jpg','.png');
                    eachPerson.picture = newImage;
                    //console.log(newImage);
                    $scope.vm.people.push(eachPerson);
                });
                this.isLoading = false;
                this.loadingText = 'Done';
            }, (reason) => {
            this.loadingText = 'Loading data failed: reason - '+reason;
            console.log(this.loadingText);
        }, (update) => {
            this.loadingText = 'Got notification: update - '+update;
            console.log(this.loadingText);
        });
    }
  }
}