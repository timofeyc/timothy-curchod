var ampApp;
(function (ampApp) {
    var Service = (function () {
        function Service($http, $q) {
            this.$http = $http;
            this.$q = $q;
        }
        Service.prototype.getPeople = function () {
            var deferred = this.$q.defer();
            var url = '/data';
            this.$http({
                url: url,
                method: 'GET'
            }).success(function (data, status, headers, config) {
                deferred.resolve(data);
            }).error(function (resp) {
                deferred.reject();
            });
            return deferred.promise;
        };
        Service.$inject = [
            '$http',
            '$q'];
        return Service;
    })();
    ampApp.Service = Service;
})(ampApp || (ampApp = {}));
