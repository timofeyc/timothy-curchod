module ampApp {
    export interface IService {
        getPeople(): ng.IPromise<any>;
    }
    export class Service {
        public static $inject = [
            '$http',
            '$q'];
            constructor(
                private $http: ng.IHttpService,
                private $q: ng.IQService) {}

        public getPeople(): ng.IPromise<any> {
            var deferred = this.$q.defer();
            var url = '/data';
            this.$http(<ng.IRequestConfig>{
                url: url,
                method: 'GET'
            }).success((data, status, headers, config) => {
                deferred.resolve(data);
            }).error((resp) => {
                deferred.reject();
            });
            return deferred.promise;
        }
    }
}
