/// <reference path="../typings/angularjs/angular.d.ts" />
/**
 * FED Test for AMP.
 * For development, this will watch and compile TypeScript files:
 * tsc -w
 * To watch and compile the scss file, run this:
 * grunt watch
 * To serve the app run:
 * node index.js
 */
module ampApp {
  'use strict';
  angular.module('ampApp', [])
    .controller('MainCtrl', MainCtrl);
  angular.module('ampApp')
    .service('Service', Service);
}