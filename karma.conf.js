
module.exports = function(config) {
    config.set({

        basePath: '',
        frameworks: ['mocha -u tdd', 'chai'],
        files: [
            'bower_components/angular/angular.js',
            'node_modules/angular-mocks/angular-mocks.js',
            'node_modules/sinon/pkg/sinon.js',
            'node_modules/chai/lib/chai.js',
            'app/app.js/',
            'app/main/main.controller.js',
            'test/main.controller.spec.js'
        ], 
        exclude: [],
        reporters: ['progress'],
        port: 9876,
        colors: true,
        logLevel: config.LOG_INFO,
        autoWatch: false,
        browsers: ['PhantomJS'],
        singleRun: true
    });
};
